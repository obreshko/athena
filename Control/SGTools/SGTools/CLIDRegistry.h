/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef SGTOOLS_CLIDREGISTRY_H
# define SGTOOLS_CLIDREGISTRY_H
/** @file CLIDRegistry.h
 * @brief  a static registry of CLID->typeName entries. NOT for general use.
 * Use ClassIDSvc instead.
 *
 * @author Paolo Calafiura <pcalafiura@lbl.gov> - ATLAS Collaboration
 *$Id: CLIDRegistry.h,v 1.2 2009-01-15 19:07:29 binet Exp $
 */


// Moved to AthenaKernel
#include "AthenaKernel/CLIDRegistry.h"


#endif // SGTOOLS_CLIDREGISTRY_H
